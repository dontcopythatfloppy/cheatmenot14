#include "stdafx.h"
#include <Windows.h>
#include <TlHelp32.h>
#include <tchar.h>
#include <iostream>
#include <fstream>
#include <string>
#include "json/json.h"
#include <time.h>

using namespace std;

HANDLE g_hProcess;

struct opponent {
	string originId;
	unsigned int personaId;
	string clubName;
	time_t established;
	Json::Value userdata;
	Json::Value formation;
};

struct squad {
	string name;
	unsigned int id;
	unsigned int rating;
	unsigned int chemistry;
};

struct card {
	unsigned int discardValue;
	unsigned int index;
	unsigned int rating;
	unsigned int playStyle;
	unsigned int contracts;
	string itemType;
	string preferredPosition;
	boolean loyaltyBonus;
	boolean untradeable;
};

void printout(const string& message)
{
	cout << message << endl;
}

void readcard(const Json::Value& js, card& c)
{
	c.discardValue = js["discardValue"].asUInt();
	c.rating = js["rating"].asUInt();
	c.playStyle = js["playStyle"].asUInt();
	c.itemType = js["itemType"].asString();
	c.preferredPosition = js["preferredPosition"].asString();
	c.loyaltyBonus = js["loyaltyBonus"].asBool();
	c.untradeable = js["untradeable"].asBool();
	c.contracts = js["contract"].asUInt();
}

void readsquad(const Json::Value& js, squad& sq)
{
	sq.name = js["squadName"].asString();
	sq.rating = js["rating"].asUInt();
	sq.chemistry = js["chemistry"].asUInt();
	sq.id = js["id"].asUInt();
}

void printError(TCHAR* msg)
{
	DWORD eNum;
	TCHAR sysMsg[256];
	TCHAR* p;

	eNum = GetLastError();
	FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
		NULL, eNum,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		sysMsg, 256, NULL);
	p = sysMsg;
	while ((*p > 31) || (*p == 9))
		++p;
	do { *p-- = 0; } while ((p >= sysMsg) &&
		((*p == '.') || (*p < 33)));
	_tprintf(TEXT("\n  WARNING: %s failed with error %d (%s)\n"), msg, eNum, sysMsg);
}

bool GetProcessPidByName(LPTSTR processName, DWORD& pid)
{
	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;
	hProcessSnap = CreateToolhelp32Snapshot(TH32CS_SNAPPROCESS, 0);
	if (hProcessSnap == INVALID_HANDLE_VALUE)
	{
		printError(TEXT("CreateToolhelp32Snapshot (of processes)"));
	}
	else
	{
		pe32.dwSize = sizeof(PROCESSENTRY32);
		if (!Process32First(hProcessSnap, &pe32))
		{
			printError(TEXT("Process32First")); // show cause of failure
			//CloseHandle(hProcessSnap);          // clean the snapshot object
		}
		else
		{
			do
			{
				if (!_tcscmp(pe32.szExeFile, processName))
				{
					CloseHandle(hProcessSnap);
					pid = pe32.th32ProcessID;
					return(true);
				}
			} while (Process32Next(hProcessSnap, &pe32));
		}

	}
	CloseHandle(hProcessSnap);
	return(false);
}

UINT_PTR GetModuleBase(LPTSTR lpModuleName, DWORD dwProcessId)
{
	MODULEENTRY32 lpModuleEntry = { 0 };
	HANDLE hSnapShot = CreateToolhelp32Snapshot(TH32CS_SNAPMODULE, dwProcessId);
	if (!hSnapShot)
		return NULL;
	lpModuleEntry.dwSize = sizeof(lpModuleEntry);
	BOOL bModule = Module32First(hSnapShot, &lpModuleEntry);
	while (bModule)
	{
		if (!_tcscmp(lpModuleEntry.szModule, lpModuleName))
		{
			CloseHandle(hSnapShot);
			return (UINT_PTR)lpModuleEntry.modBaseAddr;
		}
		bModule = Module32Next(hSnapShot, &lpModuleEntry);
	}
	CloseHandle(hSnapShot);
	return NULL;
}

UINT_PTR DeRef(UINT_PTR _uiptrPointer)
{
	UINT_PTR uiptrRet;
	if (!ReadProcessMemory(g_hProcess,
		reinterpret_cast<LPVOID>(_uiptrPointer),
		&uiptrRet,
		sizeof(uiptrRet), NULL))
	{
		return 0UL;
	}

	return uiptrRet;
}

void printhead()
{
	system("CLS");
	printout("CheatMeNot14 BETA4-3  http://bitbucket.org/dontcopythatfloppy/cheatmenot14-beta");
	printout("===============================================================================");
	printout("Detecting opponent..\n");
}


void printsquad(const squad& sq, const unsigned int& id)
{
	string s = "\t";
	if (id == sq.id)
		s += "ACTIVE!";
	//s += "\t- " + sq.name + " (rating: " + to_string(sq.rating) + ", chemistry: " + to_string(sq.chemistry) + ")\t";
	s += "\t- " + sq.name + "\t";
		
	//primi controlli su rating e chemistry
	if (sq.rating < 70)
		s += "(VLR!)\t";
	else if (sq.rating < 80)
		s += "(LR!)\t";
	if (sq.chemistry < 55)
		s += "(VLC!)";
	else if (sq.chemistry < 65)
		s += "(LC!)";
	printout(s);
}

string timeStampToHReadble(const time_t& rawtime)
{
	char buffer[30];
	struct tm timeinfo;
	localtime_s(&timeinfo, &rawtime);
	strftime(buffer, sizeof(buffer), "%d/%m/%Y", &timeinfo);
	return string(buffer);
}

void blacklist(const opponent& opp, const string& warnings) {
	Json::Reader reader;
	Json::StyledWriter writer;
	Json::Value root;
	Json::Value datenode;
	Json::Value report;
	Json::Value persona;
	string output;
	ifstream t("CheatMeNot14.json");
	ofstream fileout;
	string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());
	t.close();
	reader.parse(str, root);
	report["originId"] = opp.originId;
	report["clubName"] = opp.clubName;
	report["established"] = opp.established;
	report["reasons"] = warnings;
	//check if he's already listed
	if (root["blacklist"].isMember(to_string(opp.personaId))) {
		root["blacklist"][to_string(opp.personaId)][to_string((int)time(0))] = report;
	}
	else {
		datenode[to_string((int)time(0))] = report;
		root["blacklist"][to_string(opp.personaId)] = datenode;
	}
	output = writer.write(root);
	fileout.open("CheatMeNot14.json");
	fileout << output;
	fileout.close();
}

void whitelist(const opponent& opp) {
	Json::Reader reader;
	Json::StyledWriter writer;
	Json::Value root;
	Json::Value datenode;
	Json::Value report;
	string output;
	ifstream t("CheatMeNot14.json");
	ofstream fileout;
	string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());
	t.close();
	reader.parse(str, root);
	report["originId"] = opp.originId;
	report["clubName"] = opp.clubName;
	report["established"] = opp.established;
	if (root["whitelist"].isMember(to_string(opp.personaId))) {
		root["whitelist"][to_string(opp.personaId)][to_string((int)time(0))] = report;
	}
	else {
		datenode[to_string((int)time(0))] = report;
		root["whitelist"][to_string(opp.personaId)] = datenode;
	}
	output = writer.write(root);
	fileout.open("CheatMeNot14.json");
	fileout << output;
	fileout.close();
}

boolean inBlacklist(const unsigned int& personaId, const Json::Value& list) {
	return list["blacklist"].isMember(to_string(personaId));
}
boolean inWhitelist(const unsigned int& personaId, const Json::Value& list) {
	return list["whitelist"].isMember(to_string(personaId));
}

void loadLists(Json::Value& root, Json::Reader& reader) {
	ifstream t("CheatMeNot14.json");
	string str((std::istreambuf_iterator<char>(t)),
		std::istreambuf_iterator<char>());
	t.close();
	reader.parse(str, root);
}

wstring ExePath() {
	wchar_t buffer[MAX_PATH];
	GetModuleFileName(NULL, buffer, MAX_PATH);
	wstring::size_type pos = wstring(buffer).find_last_of(L"\\/");
	return wstring(buffer).substr(0, pos);
}

void openBrowser(const opponent& opp, const int& mode) {
	ofstream fileout;
	fileout.open("bounce.html");
	fileout << "<html><head><meta http-equiv=\"refresh\" content=\"0; URL = 'frames.html?originId=" + opp.originId + "'\"></head></html>";
	fileout.close();
	wstring bounce = L"file:///" + ExePath() + L"\\bounce.html";
	LPCWSTR wurl = bounce.c_str();
	ShellExecute(NULL, NULL, wurl, NULL, NULL, mode);
}

void readName(char* nBuffer, char* fBuffer, unsigned short n) {
	size_t i = (n / sizeof(char));
	bool firstfound = false;
	bool secondfound = false;
	while (i > 0) {
		//it should never be i = 32 since OriginID<16>|division?<1>|wtf?<?>
		if (i < 32 && nBuffer[i] == '|') {
			if (firstfound) {
				secondfound = true;
			}
			else {
				firstfound = true;
			}
		}
		i -= 1;
		fBuffer[i] = '\0';
		if (secondfound) {
			fBuffer[i] = nBuffer[i];
		}
	}
}

int _tmain(int argc, _TCHAR* argv[])
{
	bool openurl = false;
	bool oldpointer = false;
	//string url = "";
	for (int a = 0; a < argc; ++a)
	{
		if (!_tcscmp(argv[a], L"--auto-open-profile")) {
			openurl = true;
		}
		if (!_tcscmp(argv[a], L"--old-name-pointer")) {
			oldpointer = true;
		}
	}
	
	const int BASE_OFFSET = 0x46A4154;
	const int PLAYER_OFFSET1 = 0x88;
	const int PLAYER_OFFSET2 = 0xC4;
	const int PLAYER_OFFSET3 = 0x20;
	
	//const int BASE_OFFSET_B = 0x46D9364;
	const int PLAYER_OFFSET1_B = 0x6DB;

	const int BASE_OFFSET_B = 0x4694414;

	//const int DLL_OFFSET = 0x1BB0C8;
	const int DLL_OFFSET = 0x1D3B50;
	const int DLL_OFFSET1 = 0x10;
	const int DLL_OFFSET2 = 0x1C;
	const int DATA_BUFFER_SIZE = 32768;

	DWORD pid = 0;
	if (GetProcessPidByName(L"fifa14.exe", pid))
	{
		UINT_PTR base_address = GetModuleBase(L"fifa14.exe", pid);
		UINT_PTR dll_base_address = GetModuleBase(L"CardsDLLzf.dll", pid);
		if (dll_base_address != NULL)
		{
			UINT_PTR uiptrPlayer;
			char nBuffer[32];
			char fBuffer[32];
			Json::Value root;
			Json::Reader reader;
			Json::Value blackwhitelist;
			loadLists(blackwhitelist, reader);
			g_hProcess = OpenProcess(PROCESS_ALL_ACCESS, FALSE, pid);

			if (oldpointer)
				uiptrPlayer = DeRef(DeRef(DeRef(base_address + BASE_OFFSET) + PLAYER_OFFSET1) + PLAYER_OFFSET2) + PLAYER_OFFSET3;
			else
				uiptrPlayer = DeRef(base_address + BASE_OFFSET_B) + PLAYER_OFFSET1_B;

			if (ReadProcessMemory(g_hProcess, (LPVOID)(uiptrPlayer), &nBuffer, sizeof(nBuffer), NULL) != 0) {

				UINT_PTR uiptrData;
				char *dBuffer = (char *)malloc(DATA_BUFFER_SIZE);

				bool running = true;
				bool got_userdata = false;
				bool done_userdata = false;
				bool got_actives = false;
				bool done_actives = false;
				bool got_newname = false;
				bool listed = false;
				bool openprofile = false;
				bool unloaded = false;

				ATOM hk_black = GlobalAddAtom(L"CheatMeNot14_BlacklistPlayer");
				ATOM hk_white = GlobalAddAtom(L"CheatMeNot14_WhitelistPlayer");
				ATOM hk_profile = GlobalAddAtom(L"CheatMeNot14_OpenPlayerProfile");

				if (!RegisterHotKey(NULL, hk_black, MOD_ALT | MOD_NOREPEAT, 'B') ||
					!RegisterHotKey(NULL, hk_white, MOD_ALT | MOD_NOREPEAT, 'W') ||
					!RegisterHotKey(NULL, hk_profile, MOD_ALT | MOD_NOREPEAT, 'C'))
				{
					if (!RegisterHotKey(NULL, hk_black, MOD_ALT, 'B') ||
						!RegisterHotKey(NULL, hk_white, MOD_ALT, 'W') ||
						!RegisterHotKey(NULL, hk_profile, MOD_ALT, 'C'))
					{
						printError(TEXT("Couldn't register hotkeys!"));
					}

				}

				MSG msg;

				const vector<string> roles = {
					"GK", "LB", "LWB", "CB",
					"RB", "RWB", "CDM", "CM",
					"CAM", "LM", "RM", "LW",
					"RW", "CF", "LF", "RF",
					"ST", "any" };

				string active_formation;

				const vector<string> playstyles = {
					"BASIC", "SNIPER", "FINISHER", "DEADEYE",
					"MARKSMAN", "HAWK", "ARTIST", "ARCHITECT",
					"POWERHOUSE", "MAESTRO", "ENGINE",
					"SENTINEL", "GUARDIAN", "GLADIATOR",
					"BACKBONE", "ANCHOR", "HUNTER", "CATALYST",
					"SHADOW", "WALL", "SHIELD", "CAT", "GLOVE",
					"BASIC-GK" };

				/*const vector<string> formations = {
					"f442", "f442a", "f454", "f451b", "f5212",
					"f5221", "f532", "f3412", "f3421", "f343",
					"f352", "f41212", "f41212a", "f4141", "f4222",
					"f4231", "f4231a", "f4312", "f4321", "f433",
					"f433a", "f433b", "f433c", "f433d", "f4411",
					};*/

				map <string, string> expectedPosition = {
					//442
					{ "0f442", "GK" },
					{ "1f442", "RB" },
					{ "2f442", "CB" },
					{ "3f442", "CB" },
					{ "4f442", "LB" },
					{ "5f442", "RM" },
					{ "6f442", "CM" },
					{ "7f442", "CM" },
					{ "8f442", "LM" },
					{ "9f442", "ST" },
					{ "10f442", "ST" },

					//442a
					{ "0f442a", "GK" },
					{ "1f442a", "RB" },
					{ "2f442a", "CB" },
					{ "3f442a", "CB" },
					{ "4f442a", "LB" },
					{ "5f442a", "CDM" },
					{ "6f442a", "CDM" },
					{ "7f442a", "RM" },
					{ "8f442a", "LM" },
					{ "9f442a", "ST" },
					{ "10f442a", "ST" },

					//451
					{ "0f451", "GK" },
					{ "1f451", "RB" },
					{ "2f451", "CB" },
					{ "3f451", "CB" },
					{ "4f451", "LB" },
					{ "5f451", "RM" },
					{ "6f451", "CM" },
					{ "7f451", "LM" },
					{ "8f451", "CAM" },
					{ "9f451", "CAM" },
					{ "10f451", "ST" },

					//451a
					{ "0f451a", "GK" },
					{ "1f451a", "RB" },
					{ "2f451a", "CB" },
					{ "3f451a", "CB" },
					{ "4f451a", "LB" },
					{ "5f451a", "RM" },
					{ "6f451a", "CM" },
					{ "7f451a", "CM" },
					{ "8f451a", "CM" },
					{ "9f451a", "LM" },
					{ "10f451a", "ST" },

					//5212
					{ "0f5212", "GK" },
					{ "1f5212", "RWB" },
					{ "2f5212", "CB" },
					{ "3f5212", "CB" },
					{ "4f5212", "CB" },
					{ "5f5212", "LWB" },
					{ "6f5212", "CM" },
					{ "7f5212", "CM" },
					{ "8f5212", "CAM" },
					{ "9f5212", "ST" },
					{ "10f5212", "ST" },

					//5221
					{ "0f5221", "GK" },
					{ "1f5221", "RWB" },
					{ "2f5221", "CB" },
					{ "3f5221", "CB" },
					{ "4f5221", "CB" },
					{ "5f5221", "LWB" },
					{ "6f5221", "CM" },
					{ "7f5221", "CM" },
					{ "8f5221", "RW" },
					{ "9f5221", "ST" },
					{ "10f5221", "LW" },

					//532
					{ "0f532", "GK" },
					{ "1f532", "RWB" },
					{ "2f532", "CB" },
					{ "3f532", "CB" },
					{ "4f532", "CB" },
					{ "5f532", "LWB" },
					{ "6f532", "CM" },
					{ "7f532", "CM" },
					{ "8f532", "CM" },
					{ "9f532", "ST" },
					{ "10f532", "ST" },

					//3412
					{ "0f3412", "GK" },
					{ "1f3412", "CB" },
					{ "2f3412", "CB" },
					{ "3f3412", "CB" },
					{ "4f3412", "RM" },
					{ "5f3412", "CM" },
					{ "6f3412", "CM" },
					{ "7f3412", "LM" },
					{ "8f3412", "CAM" },
					{ "9f3412", "ST" },
					{ "10f3412", "ST" },

					//3421
					{ "0f3421", "GK" },
					{ "1f3421", "CB" },
					{ "2f3421", "CB" },
					{ "3f3421", "CB" },
					{ "4f3421", "RM" },
					{ "5f3421", "CM" },
					{ "6f3421", "CM" },
					{ "7f3421", "LM" },
					{ "8f3421", "RF" },
					{ "9f3421", "ST" },
					{ "10f3421", "LF" },

					//343
					{ "0f343", "GK" },
					{ "1f343", "CB" },
					{ "2f343", "CB" },
					{ "3f343", "CB" },
					{ "4f343", "RM" },
					{ "5f343", "CM" },
					{ "6f343", "CM" },
					{ "7f343", "LM" },
					{ "8f343", "RW" },
					{ "9f343", "ST" },
					{ "10f343", "LW" },

					//352
					{ "0f352", "GK" },
					{ "1f352", "CB" },
					{ "2f352", "CB" },
					{ "3f352", "CB" },
					{ "4f352", "CDM" },
					{ "5f352", "CDM" },
					{ "6f352", "RM" },
					{ "7f352", "LM" },
					{ "8f352", "CAM" },
					{ "9f352", "ST" },
					{ "10f352", "ST" },

					//41212
					{ "0f41212", "GK" },
					{ "1f41212", "RB" },
					{ "2f41212", "CB" },
					{ "3f41212", "CB" },
					{ "4f41212", "LB" },
					{ "5f41212", "CDM" },
					{ "6f41212", "RM" },
					{ "7f41212", "LM" },
					{ "8f41212", "CAM" },
					{ "9f41212", "ST" },
					{ "10f41212", "ST" },

					//41212a
					{ "0f41212a", "GK" },
					{ "1f41212a", "RB" },
					{ "2f41212a", "CB" },
					{ "3f41212a", "CB" },
					{ "4f41212a", "LB" },
					{ "5f41212a", "CDM" },
					{ "6f41212a", "CM" },
					{ "7f41212a", "CM" },
					{ "8f41212a", "CAM" },
					{ "9f41212a", "ST" },
					{ "10f41212a", "ST" },

					//4141
					{ "0f4141", "GK" },
					{ "1f4141", "RB" },
					{ "2f4141", "CB" },
					{ "3f4141", "CB" },
					{ "4f4141", "LB" },
					{ "5f4141", "CDM" },
					{ "6f4141", "RM" },
					{ "7f4141", "CM" },
					{ "8f4141", "CM" },
					{ "9f4141", "LM" },
					{ "10f4141", "ST" },

					//4222
					{ "0f4222", "GK" },
					{ "1f4222", "RB" },
					{ "2f4222", "CB" },
					{ "3f4222", "CB" },
					{ "4f4222", "LB" },
					{ "5f4222", "CDM" },
					{ "6f4222", "CDM" },
					{ "7f4222", "CAM" },
					{ "8f4222", "CAM" },
					{ "9f4222", "ST" },
					{ "10f4222", "ST" },

					//4231
					{ "0f4231", "GK" },
					{ "1f4231", "RB" },
					{ "2f4231", "CB" },
					{ "3f4231", "CB" },
					{ "4f4231", "LB" },
					{ "5f4231", "CDM" },
					{ "6f4231", "CDM" },
					{ "7f4231", "CAM" },
					{ "8f4231", "CAM" },
					{ "9f4231", "CAM" },
					{ "10f4231", "ST" },

					//4231a
					{ "0f4231a", "GK" },
					{ "1f4231a", "RB" },
					{ "2f4231a", "CB" },
					{ "3f4231a", "CB" },
					{ "4f4231a", "LB" },
					{ "5f4231a", "CDM" },
					{ "6f4231a", "CDM" },
					{ "7f4231a", "RM" },
					{ "8f4231a", "LM" },
					{ "9f4231a", "CAM" },
					{ "10f4231a", "ST" },

					//4312
					{ "0f4312", "GK" },
					{ "1f4312", "RB" },
					{ "2f4312", "CB" },
					{ "3f4312", "CB" },
					{ "4f4312", "LB" },
					{ "5f4312", "CM" },
					{ "6f4312", "CM" },
					{ "7f4312", "CM" },
					{ "8f4312", "CAM" },
					{ "9f4312", "ST" },
					{ "10f4312", "ST" },

					//4321
					{ "0f4321", "GK" },
					{ "1f4321", "RB" },
					{ "2f4321", "CB" },
					{ "3f4321", "CB" },
					{ "4f4321", "LB" },
					{ "5f4321", "CM" },
					{ "6f4321", "CM" },
					{ "7f4321", "CM" },
					{ "8f4321", "RF" },
					{ "9f4321", "LF" },
					{ "10f4321", "ST" },

					//433
					{ "0f433", "GK" },
					{ "1f433", "RB" },
					{ "2f433", "CB" },
					{ "3f433", "CB" },
					{ "4f433", "LB" },
					{ "5f433", "CM" },
					{ "6f433", "CM" },
					{ "7f433", "CM" },
					{ "8f433", "RW" },
					{ "9f433", "ST" },
					{ "10f433", "LW" },

					//433a
					{ "0f433a", "GK" },
					{ "1f433a", "RB" },
					{ "2f433a", "CB" },
					{ "3f433a", "CB" },
					{ "4f433a", "LB" },
					{ "5f433a", "CDM" },
					{ "6f433a", "CM" },
					{ "7f433a", "CM" },
					{ "8f433a", "RW" },
					{ "9f433a", "ST" },
					{ "10f433a", "LW" },

					//433b
					{ "0f433b", "GK" },
					{ "1f433b", "RB" },
					{ "2f433b", "CB" },
					{ "3f433b", "CB" },
					{ "4f433b", "LB" },
					{ "5f433b", "CDM" },
					{ "6f433b", "CDM" },
					{ "7f433b", "CM" },
					{ "8f433b", "RW" },
					{ "9f433b", "ST" },
					{ "10f433b", "LW" },

					//433c
					{ "0f433c", "GK" },
					{ "1f433c", "RB" },
					{ "2f433c", "CB" },
					{ "3f433c", "CB" },
					{ "4f433c", "LB" },
					{ "5f433c", "CM" },
					{ "6f433c", "CM" },
					{ "7f433c", "CAM" },
					{ "8f433c", "RW" },
					{ "9f433c", "ST" },
					{ "10f433c", "LW" },

					//433d
					{ "0f433d", "GK" },
					{ "1f433d", "RB" },
					{ "2f433d", "CB" },
					{ "3f433d", "CB" },
					{ "4f433d", "LB" },
					{ "5f433d", "CM" },
					{ "6f433d", "CDM" },
					{ "7f433d", "CM" },
					{ "8f433d", "CF" },
					{ "9f433d", "RW" },
					{ "10f433d", "LW" },

					//4411
					{ "0f4411", "GK" },
					{ "1f4411", "RB" },
					{ "2f4411", "CB" },
					{ "3f4411", "CB" },
					{ "4f4411", "LB" },
					{ "5f4411", "RM" },
					{ "6f4411", "CM" },
					{ "7f4411", "CM" },
					{ "8f4411", "LM" },
					{ "9f4411", "CF" },
					{ "10f4411", "ST" },
				};


				/* not using these or the motm/tots/toty cards would be unfairly revealed

				const unsigned int tiersUpperBounds[] = {
				20, 50, 112, 260, 400, 795, 1300,
				5200, 13000, 65000 };

				const vector<string> tiers = {
				"bronze", "rare bronze", "silver", "rare silver",
				"gold", "rare gold", "special bronze", "special silver", "special gold", "tier error" };
				
				*/

				const vector<string> tiers = {
					"bronze", "rare bronze", "silver", "rare silver",
					"gold", "rare gold or better" };

				const unsigned int tiersUpperBounds[] = {
					20, 50, 112, 260, 400, 65000 };

				map <string, int> stylesCount;
				map <string, int> rolesCount;
				map <string, int> tiersCount;

				int loyals = 0;
				int untradeables = 0;
				int active_rate = 0;
				int active_chem = 0;
				int unloyals_with_basic_chem = 0;
				opponent opp;
				readName(nBuffer, fBuffer, sizeof(nBuffer));
				opp.originId.assign(fBuffer);
				string warnings = "";

				printhead();

				while (running)
				{
					if (oldpointer)
						uiptrPlayer = DeRef(DeRef(DeRef(base_address + BASE_OFFSET) + PLAYER_OFFSET1) + PLAYER_OFFSET2) + PLAYER_OFFSET3;
					else
						uiptrPlayer = DeRef(base_address + BASE_OFFSET_B) + PLAYER_OFFSET1_B;
					if (ReadProcessMemory(g_hProcess, (LPVOID)(uiptrPlayer), &nBuffer, sizeof(nBuffer), NULL) != 0) {
						readName(nBuffer, fBuffer, sizeof(nBuffer));
						if (opp.originId.compare(fBuffer) != 0) {

							//new opponent, resetting everything
							for (size_t i = 0; i < roles.size(); i++) {
								rolesCount[roles[i]] = 0;
							}
							for (size_t i = 0; i < playstyles.size(); i++) {
								stylesCount[playstyles[i]] = 0;
							}
							for (size_t i = 0; i < tiers.size(); i++) {
								tiersCount[tiers[i]] = 0;
							}

							active_rate = 0;
							active_chem = 0;
							loyals = 0;
							unloyals_with_basic_chem = 0;
							untradeables = 0;
							warnings = "";

							opp.originId.assign(fBuffer);

							openprofile = true;
							//adding profile url to the clipboard - BETA3
							/*if (OpenClipboard(NULL)) {
								EmptyClipboard();
								HGLOBAL hClipboardData;
								hClipboardData = GlobalAlloc(GMEM_MOVEABLE, url.size() + 1);
								if (!hClipboardData) {
								CloseClipboard();
								}
								else {
								memcpy(GlobalLock(hClipboardData), url.c_str(), url.size());
								GlobalUnlock(hClipboardData);
								SetClipboardData(CF_TEXT, hClipboardData);
								CloseClipboard();
								GlobalFree(hClipboardData);
								}
								}*/

							//open browser automatically
							if (openurl) {
								openBrowser(opp, SW_SHOWNOACTIVATE);
							}

							got_newname = true;
							got_userdata = false;
							got_actives = false;
							done_userdata = false;
							done_actives = false;
							listed = false;
						}
						//reading data memory area if we haven't received formation and clubdata
						if (!(done_actives && done_userdata)) {
							uiptrData = DeRef(DeRef(DeRef(dll_base_address + DLL_OFFSET) + DLL_OFFSET1) + DLL_OFFSET2);
							if (ReadProcessMemory(g_hProcess, (LPVOID)(uiptrData), dBuffer, DATA_BUFFER_SIZE, NULL) != false) {
								if (reader.parse(dBuffer, root)) {
									//check if we got clubdata
									if (!got_userdata && root.isMember("user")) {
										opp.userdata = root["user"][0];
										got_userdata = true;
									}
									//check if we got formation
									if (!got_actives && root.isMember("actives")) {
										if (opp.personaId == root["personaId"].asUInt()) {
											opp.formation = root["players"];
											active_formation.assign(root["formation"].asString());
											got_actives = true;
										}
									}
								}
							}
							else {
								//if reading memory fails check if ultimate team is still loaded
								if (GetModuleBase(L"CardsDLLzf.dll", pid) == NULL) {
									//ultimate team is not loaded anymore, halt here
									//pretend everything has been read and listed, waiting for a new username
									done_actives = true;
									done_userdata = true;
									openprofile = false;
									if (!unloaded) {
										printout("\n\tWarning: Ultimate Team unloaded, waiting for it to get reloaded.");
									}
									listed = true;
									unloaded = true;

								}
								else {
									//if it's still loaded, update its base address, maybe it got unloaded
									if (dll_base_address != GetModuleBase(L"CardsDLLzf.dll", pid)) {
										unloaded = false;
										dll_base_address = GetModuleBase(L"CardsDLLzf.dll", pid);
										printout("\n\tInfo: Ultimate Team succesfully reloaded, if something doesn't work try restarting this tool.");
									}
								}
							}
						}

						if (got_newname) {
							printhead();
							printout("\tOriginID: " + opp.originId + " (ALT+c to check his FUT profile)");
							got_newname = false;
						}

						//printing & checking clubdata
						if (got_userdata && !done_userdata) {
							done_userdata = true;
							opp.personaId = opp.userdata["personaId"].asUInt();
							opp.clubName = opp.userdata["clubName"].asString();
							
							//type is not convertible to int or uint apparently, must stoi(string)
							opp.established = (time_t)stoi(opp.userdata["established"].asString());
							//printout("\tPersonaID: " + to_string(opp.personaId)); 
							printout("\tClub name: " + opp.clubName +" (" + timeStampToHReadble(opp.established)+")");
							printout("\nSquadlist:");
							int activeId = opp.userdata["squadList"]["activeSquadId"].asUInt();
							for (Json::Value jsquad : opp.userdata["squadList"]["squad"]) {
								squad sq;
								readsquad(jsquad, sq);
								printsquad(sq, activeId);
								if (sq.id == activeId) {
									active_rate = sq.rating;
									active_chem = sq.chemistry;
								}
							}
							//lists checks
							if (inBlacklist(opp.personaId, blackwhitelist))
								printout("\n\tBLACKLISTED OPPONENT! Back off or risk it.");
							else if (inWhitelist(opp.personaId, blackwhitelist))
								printout("\n\tWHITELISTED OPPONENT! We should be fine.");
						}
						//printing and checking formation
						if (got_actives && !done_actives) {
							done_actives = true;
							for (Json::Value jscard : opp.formation) {
								card c;
								c.index = jscard["index"].asUInt();
								readcard(jscard["itemData"], c);
								if (c.index < 11 && c.preferredPosition.compare("any") != 0) {
									//checking the roles of the starting players
									if (expectedPosition[to_string(c.index) + active_formation].compare(c.preferredPosition) != 0)
										warnings += "\t- detected a " + c.preferredPosition + " as " + expectedPosition[to_string(c.index) + active_formation] + "!\n";// (" + to_string(c.index) + active_formation + " ? )\n";
									//counting chemistry styles
									stylesCount[playstyles[c.playStyle - 250]]++;
									if (c.loyaltyBonus)
										loyals++;
									else if (c.playStyle == 250 || c.playStyle == 273)
										unloyals_with_basic_chem++;
									if (c.untradeable)
										untradeables++;
									else for (size_t i = 0; i < tiers.size(); i++) {
										if (c.discardValue < tiersUpperBounds[i]) {
											tiersCount[tiers[i]]++;
											break;
										}
									}
								}
								//counting empty reserves slots
								else if (c.preferredPosition.compare("any") == 0)
									rolesCount[c.preferredPosition]++;
							}

							//printing out analyzed data and warnings
							printout("\nStarters:");
							for (size_t i = 0; i < tiers.size(); i++) {
								if (tiersCount[tiers[i]] > 0)
									printout("\t" + to_string(tiersCount[tiers[i]]) + "\t" + tiers[i]);
							}
							if (untradeables > 0)
								printout("\t" + to_string(untradeables) + "\tuntradeable");

							printout("\t" + to_string(loyals) + "\tloyal players");
							printout("\nWarnings:");

							//rating check active squad
							if (active_rate < 70)
								warnings += "\t- active squad has very low rating! (" + to_string(active_rate) + ")\n";
							else if (active_rate < 80)
								warnings += "\t- active squad has low rating! (" + to_string(active_rate) + ")\n";
							//chem check active squad
							if (active_chem < 55)
								warnings += "\t- active squad has very low chemistry! (" + to_string(active_chem) + ")\n";
							else if (active_chem < 65)
								warnings += "\t- active squad has low chemistry! (" + to_string(active_chem) + ")\n";

							//check tiers
							//bronze
							if (tiersCount["bronze"] + tiersCount["rare bronze"] > 0)
								warnings += "\t- " + to_string(tiersCount["bronze"] + tiersCount["rare bronze"]) + " bronze player(s) detected!\n";
							//silver
							if (tiersCount["silver"] + tiersCount["rare silver"] > 0)
								warnings += "\t- " + to_string(tiersCount["silver"] + tiersCount["rare silver"]) + " silver player(s) detected!\n";
							//gold
							if (tiersCount["gold"] > 2)
								warnings += "\t- " + to_string(tiersCount["gold"]) + " non-rare gold players.\n";
							//untradeables warn
							if (untradeables > 1)
								warnings += "\t- " + to_string(untradeables) + " untradeable player(s).\n";
							//chem styles
							if (stylesCount["BASIC"] + stylesCount["BASIC-GK"] > 0)
								warnings += "\t- " + to_string(stylesCount["BASIC"] + stylesCount["BASIC-GK"]) + " basic chem player(s)! (" + to_string(unloyals_with_basic_chem) + " of which unloyal(s))\n";
							//empty slots
							if (rolesCount["any"] > 0)
								warnings += "\t- " + to_string(rolesCount["any"]) + " empty reserve slot(s) detected!\n";
							printout(warnings);
							printout("ALT+b to blacklist, ALT+w to whitelist or just do nothing if you're unsure.");
						}
					}
					else {
						running = false;
						printError(TEXT("Couldn't read name memory address! Is FIFA 14 still running?"));
					}

					//input
					if (MsgWaitForMultipleObjects(0, NULL, FALSE, 1000, QS_ALLEVENTS) == WAIT_OBJECT_0) {
						while (PeekMessage(&msg, 0, 0, 0, PM_REMOVE)) {
							if (openprofile) {
								if (msg.message == WM_HOTKEY && msg.wParam == hk_profile) {
									openBrowser(opp, SW_SHOW);
								}
							}
							if (done_userdata && done_actives && !listed) {
								if (msg.message == WM_HOTKEY && msg.wParam == hk_black) {
									blacklist(opp, warnings);
									printout("\n\tOpponent blacklisted.");
									listed = true;
									loadLists(blackwhitelist, reader);
								}
								if (msg.message == WM_HOTKEY && msg.wParam == hk_white){
									whitelist(opp);
									printout("\n\tOpponent whitelisted.");
									listed = true;
									loadLists(blackwhitelist, reader);
								}
							}
						}
					}

				}
				std::free(dBuffer);
				dBuffer = nullptr;
			}
			else {
				printError(TEXT("Couldn't read name memory address! (1) Please report this error."));
			}
		}
		else
		{
			printError(TEXT("Ultimate Team is not loaded! Please get into Ultimate Team before running this tool."));
		}
	}
	else
	{
		printError(TEXT("FIFA14 is not running! Please open FIFA14 and get into Ultimate Team before running this tool."));
	}
	system("pause");
	return 0;
}